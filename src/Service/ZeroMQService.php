<?php

namespace App\Service;

use ZMQ;
use ZMQContext;
use ZMQSocket;

class ZeroMQService
{
    const TYPE_MESSAGE = 'message';
    const TYPE_ONLINE = 'online';
    const TYPE_OFFLINE = 'offline';
    const SUBSCRIBER_ALL = 'all';

    /**
     * @param string $subscriber
     * @param string $type
     * @param array $object
     * @param integer $port
     *
     * @return ZMQSocket
     */
    public function publish($subscriber, $type, array $object = [], $port = 5555)
    {
        $context = new ZMQContext();
        $socket = $context->getSocket(ZMQ::SOCKET_PUSH, 'sf4');
        $socket->connect("tcp://localhost:" . $port);

        $data = [
            'subscriber' => $subscriber,
            'type' => $type,
            'object' => $object
        ];

        return $socket->send(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE));
    }
}