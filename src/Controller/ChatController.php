<?php

namespace App\Controller;

use App\Entity\ChatMessage;
use App\Entity\User;
use App\Service\ZeroMQService;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ZMQ;
use ZMQContext;

/**
 * @Security("has_role('user')")
 */
class ChatController extends Controller
{
    /**
     * @Route("/chat", name="chat")
     */
    public function chat(Request $request)
    {
        $with = $request->query->get('with');
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $conversations = $em->getRepository('App:ChatMessage')->findConversations($user->getId());
        $userIds[] = [$user->getId()];
        foreach ($conversations as $conv) {
            $userIds[] = $conv['with']['id'];
        }
        $otherUsers =  $em->getRepository('App:User')->findUsersExcept($userIds);

        $params = [
            'with' => $with,
            'otherUsers' => $otherUsers,
            'conversations' => json_encode($conversations, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES),
        ];

        if ($with) {
            $params['withUser'] = $em->getRepository('App:User')->findOneById($with);
        }

        return $this->render('chat/chat.html.twig', $params);
    }

    /**
     * @Route("/chat/post-message", name="chat_post_message")
     */
    public function postMessage(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $request->request->replace($data);

        $message = $request->request->get('message');
        $to = $request->request->get('to');

        $userFrom = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        /** @var User $userTo */
        $userTo = $em->getRepository("App:User")->findOneById($to);

        if (!$userFrom) {
            new JsonResponse(['error' => 'User is not authenticated'], 400);
        }
        if (!$userTo) {
            new JsonResponse(['error' => 'To user is not found'], 400);
        }

        $chatMessage = new ChatMessage();
        $chatMessage->setFrom($userFrom);
        $chatMessage->setTo($userTo);
        $chatMessage->setMessage($message);
        $em->persist($chatMessage);
        $em->flush();

        $em->getRepository('App:ChatCounter')->countMessage($chatMessage);

        $arrayMessage = $chatMessage->toArray();
        $this->get('zeromq')->publish($userTo->getId(), ZeroMQService::TYPE_MESSAGE, $arrayMessage);

        return new JsonResponse(['message' => $arrayMessage]);
    }
}