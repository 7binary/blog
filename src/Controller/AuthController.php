<?php

namespace App\Controller;

use App\Form\UserType;
use App\Entity\User;
use App\Service\ZeroMQService;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class AuthController extends Controller
{
    /**
     * @Route("/register", name="register")
     */
    public function registerAction(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        if ($this->userIsLogged()) {
            return $this->redirectToRoute('homepage');
        }

        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            $user->addRole(User::ROLE_USER);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'Вы успешно зарегистрировались на сайте!');

            return $this->redirectToRoute('homepage');
        }

        return $this->render('auth/register.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/entry", name="entry")
     */
    public function loginAction(Request $request)
    {
        if ($this->userIsLogged()) {
            return $this->redirectToRoute('homepage');
        }

        $username = $request->request->get('username');
        $password = $request->request->get('password');

        $params = [
            'title' => 'Войти на сайт',
            'username' => $username,
            'password' => $password,
            'error' => null,
        ];

        if ($request->isMethod('POST')) {
            $em = $this->getDoctrine()->getManager();

            /** @var User $user */
            if ($user = $em->getRepository('App:User')->loadUserByUsername($username)) {
                $encoder = new BCryptPasswordEncoder(12);
                if ($encoder->isPasswordValid($user->getPassword(), $password, $user->getSalt())) {
                    $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
                    $this->get('session')->set('_security_main', serialize($token));
                    $this->get('security.token_storage')->setToken($token);
                    // Fire the login event manually
                    $event = new InteractiveLoginEvent($request, $token);
                    $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

                    $now = new \DateTime('now');
                    $user->setLoggedAt($now);
                    $user->setOnlineAt($now);
                    $em->flush();

                    # вошел - оповести остальных
                    $this->get('zeromq')->publish(
                        ZeroMQService::SUBSCRIBER_ALL,
                        ZeroMQService::TYPE_ONLINE,
                        ['userId' => $user->getId()]
                    );

                    return $this->redirectToRoute('homepage');
                }
            }

            $params['error'] = 'Неверный логин или пароль';
        }

        return $this->render('auth/entry.html.twig', $params);
    }

    private function userIsLogged()
    {
        $securityContext = $this->container->get('security.authorization_checker');

        return $securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED');
    }
}