<?php

namespace App\Command;

use App\Model\Chat;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use ZMQ;

class ChatCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('chat');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('=> chat started');

        $loop   = \React\EventLoop\Factory::create();
        $chat = new Chat($this->getContainer());

        // Listen for the web server to make a ZeroMQ push after an ajax request
        $context = new \React\ZMQ\Context($loop);
        /** @var \React\ZMQ\SocketWrapper $pull */
        $pull = $context->getSocket(ZMQ::SOCKET_PULL);
        $pull->bind('tcp://127.0.0.1:5555'); // Binding to 127.0.0.1 means the only client that can connect is itself
        $pull->on('message', array($chat, 'onEvent'));

        // Set up our WebSocket server for clients wanting real-time updates
        $webSock = new \React\Socket\Server('0.0.0.0:8080', $loop); // Binding to 0.0.0.0 means remotes can connect
        $webServer = new \Ratchet\Server\IoServer(
            new \Ratchet\Http\HttpServer(
                new \Ratchet\WebSocket\WsServer(
                    new \Ratchet\Wamp\WampServer(
                        $chat
                    )
                )
            ),
            $webSock
        );

        $loop->run();

        $output->writeln('+++ chat finished');
    }
}