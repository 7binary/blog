<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="chat_counters")
 * @ORM\Entity(repositoryClass="App\Repository\ChatCounterRepository")
 */
class ChatCounter extends BaseEntity
{
    /**
     * @ORM\Column(type="integer")
     */
    private $counter = 0;

    /** @ORM\ManyToOne(targetEntity="User", inversedBy="countersFrom") */
    private $from;

    /** @ORM\ManyToOne(targetEntity="User", inversedBy="countersTo") */
    private $to;

    /**
     * @return mixed
     */
    public function getCounter()
    {
        return $this->counter;
    }

    /**
     * @param mixed $counter
     */
    public function setCounter($counter)
    {
        $this->counter = $counter;
    }

    /**
     * @return mixed
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param mixed $from
     */
    public function setFrom($from)
    {
        $this->from = $from;
    }

    /**
     * @return mixed
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param mixed $to
     */
    public function setTo($to)
    {
        $this->to = $to;
    }
}
