<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;

/**
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User extends BaseEntity implements AdvancedUserInterface, \Serializable
{
    const ROLE_USER = 'user';
    const ROLE_ADMIN = 'admin';

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=12, unique=true, nullable=true)
     */
    private $phone;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(max=4096)
     */
    private $plainPassword;

    /**
     * The below length depends on the "algorithm" you use for encoding
     * the password, but this works well with bcrypt.
     *
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /** @ORM\OneToMany(targetEntity="ChatMessage", mappedBy="to") */
    private $messagesTo;

    /** @ORM\OneToMany(targetEntity="ChatMessage", mappedBy="from") */
    private $messagesFrom;

    /** @ORM\OneToMany(targetEntity="ChatCounter", mappedBy="to") */
    private $countersTo;

    /** @ORM\OneToMany(targetEntity="ChatCounter", mappedBy="from") */
    private $countersFrom;

    /**
     * @ORM\Column(type="simple_array")
     */
    private $roles;

    /**
     * @ORM\Column(name="logged_at", type = "datetime", nullable=true)
     */
    protected $loggedAt;

    /**
     * @ORM\Column(name="online_at", type = "datetime", nullable=true)
     */
    protected $onlineAt;

    public function __construct()
    {
        $this->isActive = true;
        $this->messagesTo = new ArrayCollection();
        $this->messagesFrom = new ArrayCollection();
        $this->countersTo = new ArrayCollection();
        $this->countersFrom = new ArrayCollection();

        // may not be needed, see section on salt below
        // $this->salt = md5(uniqid('', true));
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->email;
    }

    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param mixed $plainPassword
     */
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
    }

    /**
     * @return mixed
     */
    public function getisActive()
    {
        return $this->isActive;
    }

    /**
     * @param mixed $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    public function eraseCredentials()
    {
    }

    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->email,
            $this->password,
            $this->isActive,
            $this->roles,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->email,
            $this->password,
            $this->isActive,
            $this->roles,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);
    }

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return $this->isActive;
    }

    /**
     * @return mixed
     */
    public function getRoles()
    {
        return empty($this->roles) ? [] : $this->roles;
    }

    /**
     * @param mixed $roles
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;
    }

    public function addRole($roleAdded)
    {
        $roles = $this->getRoles();
        $hasRole = false;

        if (!empty($roles)) {
            foreach ($roles as $role) {
                if ($roleAdded == $role) {
                    $hasRole = true;
                    break;
                }
            }
        }

        if ($hasRole == false) {
            $roles[] = $roleAdded;
            $this->setRoles($roles);
            return true;
        }

        return false;
    }

    public function removeRole($roleRemove)
    {
        $roles = $this->getRoles();
        $rolesFiltered = array_filter($roles, function ($role) use ($roleRemove) {
            return $role != $roleRemove;
        });

        if (count($rolesFiltered) < count($roles)) {
            $this->setRoles($rolesFiltered);
            return true;
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function getMessagesTo()
    {
        return $this->messagesTo;
    }

    /**
     * @param mixed $messagesTo
     */
    public function setMessagesTo($messagesTo)
    {
        $this->messagesTo = $messagesTo;
    }

    /**
     * @return mixed
     */
    public function getMessagesFrom()
    {
        return $this->messagesFrom;
    }

    /**
     * @param mixed $messagesFrom
     */
    public function setMessagesFrom($messagesFrom)
    {
        $this->messagesFrom = $messagesFrom;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getCountersTo()
    {
        return $this->countersTo;
    }

    /**
     * @param mixed $countersTo
     */
    public function setCountersTo($countersTo)
    {
        $this->countersTo = $countersTo;
    }

    /**
     * @return mixed
     */
    public function getCountersFrom()
    {
        return $this->countersFrom;
    }

    /**
     * @param mixed $countersFrom
     */
    public function setCountersFrom($countersFrom)
    {
        $this->countersFrom = $countersFrom;
    }

    /**
     * @return mixed
     */
    public function getLoggedAt()
    {
        return $this->loggedAt;
    }

    /**
     * @param mixed $loggedAt
     */
    public function setLoggedAt($loggedAt)
    {
        $this->loggedAt = $loggedAt;
    }

    /**
     * @return mixed
     */
    public function getOnlineAt()
    {
        return $this->onlineAt;
    }

    /**
     * @param mixed $onlineAt
     */
    public function setOnlineAt($onlineAt)
    {
        $this->onlineAt = $onlineAt;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }
}