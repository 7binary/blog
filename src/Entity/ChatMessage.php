<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="chat_messages")
 * @ORM\Entity(repositoryClass="App\Repository\ChatMessageRepository")
 */
class ChatMessage extends BaseEntity
{
    /**
     * @ORM\Column(type="text")
     */
    private $message;

    /** @ORM\ManyToOne(targetEntity="User", inversedBy="messagesFrom") */
    private $from;

    /** @ORM\ManyToOne(targetEntity="User", inversedBy="messagesTo") */
    private $to;

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return User
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param mixed $from
     */
    public function setFrom($from)
    {
        $this->from = $from;
    }

    /**
     * @return User
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param mixed $to
     */
    public function setTo($to)
    {
        $this->to = $to;
    }

    public function toArray()
    {
        $userFrom = $this->getFrom();
        $userTo = $this->getTo();

        return [
            'id' => $this->getId(),
            'message' => $this->getMessage(),
            'userFromId' => $userFrom->getId(),
            'userFromName' => $userFrom->getName(),
            'userToId' => $userTo->getId(),
            'userToName' => $userTo->getName(),
            'created' => $this->getCreated()->format('d.m.Y H:i'),
        ];
    }
}
