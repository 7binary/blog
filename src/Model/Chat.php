<?php

namespace App\Model;

use Ratchet\ConnectionInterface;
use Ratchet\Wamp\WampServerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Chat implements WampServerInterface
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * A lookup of all the topics clients have subscribed to
     */
    protected $subscribedTopics = array();

    public function onSubscribe(ConnectionInterface $conn, $message)
    {
        $this->subscribedTopics[$message->getId()] = $message;
    }

    /**
     * @param string JSON'ified string we'll receive from ZeroMQ
     */
    public function onEvent($entry)
    {
        $entryData = json_decode($entry, true);

        if (!isset($entryData['subscriber']) || !array_key_exists($entryData['subscriber'], $this->subscribedTopics)) {
            return;
        }

        $message = $this->subscribedTopics[$entryData['subscriber']];

        // re-send the data to all the clients subscribed to that category
        $message->broadcast($entryData);
    }

    /* The rest of our methods were as they were, omitted from docs to save space */

    public function onUnSubscribe(ConnectionInterface $conn, $message)
    {
    }

    public function onOpen(ConnectionInterface $conn)
    {
    }

    public function onClose(ConnectionInterface $conn)
    {
    }

    public function onCall(ConnectionInterface $conn, $id, $topic, array $params)
    {
        // In this application if clients send data it's because the user hacked around in console
        $conn->callError($id, $topic, 'You are not allowed to make calls')->close();
    }

    public function onPublish(ConnectionInterface $conn, $topic, $event, array $exclude, array $eligible)
    {
        if (!is_array($event) || empty($event['type'])) {
            return;
        }

        if ($event['type'] == 'readed' && !empty($event['userId']) && !empty($event['withId'])) {
            $this->container->get('doctrine')
                ->getRepository('App:ChatCounter')
                ->setReaded(intval($event['userId']), intval($event['withId']));
        }
        elseif ($event['type'] == 'online' && !empty($event['userId'])) {
            $this->container->get('doctrine')
                ->getRepository('App:User')
                ->setOnline(intval($event['userId']));
        }

        // In this application if clients send data it's because the user hacked around in console
        //$conn->close();
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
    }
}