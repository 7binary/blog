<?php

namespace App\Repository;

use App\Entity\ChatMessage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ChatMessageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ChatMessage::class);
    }

    public function findConversations($userId)
    {
        $conversations = [];

        # находим счетчик новых сообщений переписки
        $counters = $this->_em->getRepository('App:ChatCounter')->findByUser($userId);

        # находим все сообщения переписки для участника
        $rows = $this->_em->createQuery("
            SELECT m.message, m.created, m.id,
              userFrom.id userFromId, userFrom.name userFromName, 
              userTo.id userToId, userTo.name userToName
            FROM App:ChatMessage m
            INNER JOIN m.from userFrom
            INNER JOIN m.to userTo
            WHERE userFrom.id = :userId 
              OR userTo.id = :userId
            ORDER BY m.created DESC
        ")->setParameter('userId', $userId)
            ->getResult();

        foreach ($rows as $row) {
            $key = ($row['userToId'] == $userId) ? $row['userFromId'] : $row['userToId'];
            if (!isset($conversations[$key])) {
                $with = $row['userToId'] == $userId
                    ? ['id' => $row['userFromId'], 'name' => $row['userFromName']]
                    : ['id' => $row['userToId'], 'name' => $row['userToName']];
                $conversations[$key] = [
                    'with' => $with,
                    'lastMessage' => null,
                    'messages' => [],
                    'newMessages' => 0,
                ];
            }
            $row['created'] = $row['created']->format('d.m.Y H:i');
            $conversations[$key]['messages'][] = $row;
        }

        foreach ($conversations as $userId => &$data) {
            $data['lastMessage'] = $data['messages'][0];
            $data['messages'] = array_reverse($data['messages']);
        }

        # находим счетчик новых сообщений переписки
        if (!empty($counters)) {
            foreach ($counters as $userFromId => $counter) {
                if (isset($conversations[$userFromId])) {
                    $conversations[$userFromId]['newMessages'] = $counter;
                }
            }
        }

        return array_values($conversations);
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('c')
            ->where('c.something = :value')->setParameter('value', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
