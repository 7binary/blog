<?php

namespace App\Repository;

use App\Entity\ChatCounter;
use App\Entity\ChatMessage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ChatCounterRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ChatCounter::class);
    }

    public function setReaded($userId, $withId)
    {
        $stmt = $this->_em->getConnection()
            ->prepare("UPDATE chat_counters SET counter = 0 WHERE to_id = :to AND from_id = :from");
        $stmt->bindParam('to', $userId);
        $stmt->bindParam('from', $withId);
        $stmt->execute();
    }

    public function findbyUser($userId)
    {
        $stmt = $this->_em->getConnection()
            ->prepare("SELECT counter, from_id, to_id FROM chat_counters WHERE to_id = :userId");
        $stmt->bindParam('userId', $userId);
        $stmt->execute();
        $rows = $stmt->fetchAll();

        $counters = [];
        foreach ($rows as $row) {
            $key = $row['from_id'];
            $counters[$key] = intval($row['counter']);
        }

        return $counters;
    }

    public function countMessage(ChatMessage $message)
    {
        $userFrom = $message->getFrom();
        $userTo = $message->getTo();

        /** @var ChatCounter $counter */
        $counter = $this->_em->createQuery("
            SELECT c FROM App:ChatCounter c WHERE c.from = :from AND c.to = :to
        ")->setParameter('from', $userFrom->getId())
            ->setParameter('to', $userTo->getId())
            ->getOneOrNullResult();

        if ($counter === null) {
            $counter = new ChatCounter();
            $counter->setFrom($userFrom);
            $counter->setTo($userTo);
            $counter->setCounter(1);
            $this->_em->persist($counter);
            $this->_em->flush($counter);
        }
        else {
            $counter->setCounter($counter->getCounter() + 1);
            $this->_em->flush($counter);
        }
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('c')
            ->where('c.something = :value')->setParameter('value', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
