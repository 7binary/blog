<?php

namespace App\Repository;

use App\Entity\User;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository implements UserLoaderInterface
{
    public function findUsersExcept(array $ids)
    {
        $qb = $this->createQueryBuilder('u')->orderBy('u.name', 'ASC');

        if (!empty($ids)) {
            $qb->andWhere('u.id NOT IN (:ids)')->setParameter('ids', $ids);
        }

        return $qb->getQuery()->getResult();
    }

    public function loadUserByUsername($username)
    {
        return $this->createQueryBuilder('u')
            ->where('u.email = :username OR u.phone = :username')
            ->setParameter('username', $username)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function setOnline($userId) {
        /** @var User $user */
        if ($user = $this->findOneBy(['id' => $userId])) {
            $now = new \DateTime('now');
            $user->setOnlineAt($now);
            $this->_em->flush($user);
        }
    }
}